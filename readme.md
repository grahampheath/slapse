# Slapse

Short for "smear" or "slide" and "time lapse".

It produces in image from a video. The exact process is:

1. Extract frames from a video.
2. Crop the frame to a single pixel wide* column in the middle of the image.
3. Assemble the slices from left to right.

What results is a 2d image where the vertical axis is light (same as most pictures), and the horizontal axis is time.

*: Adjustable

[![A CTA train standing out on a background of smeared colors](https://bitbucket.org/grahampheath/slapse/raw/543e7b96e674f4d7d58497334de4344b43dc3634/demo/freight.jpg)](https://bitbucket.org/grahampheath/slapse/raw/0e9d2ded662afbb4a33e5b1766bf530b1150bd0c/out-2021-5-17-6-57-39.jpg)

Generated from high speed footage of a freight train out my window.

---

[![360 degree view, but a little different](https://bitbucket.org/grahampheath/slapse/raw/788c74cf3f26f23dd72398c98583f958ee06aefb/demo/wiscy.jpg)](https://bitbucket.org/grahampheath/slapse/raw/788c74cf3f26f23dd72398c98583f958ee06aefb/demo/wiscy.jpg)

The view from a hill top in Wisconsin.

---

[![A CTA train standing out on a background of smeared colors](https://bitbucket.org/grahampheath/slapse/raw/543e7b96e674f4d7d58497334de4344b43dc3634/demo/cta.jpg)](https://bitbucket.org/grahampheath/slapse/raw/0e9d2ded662afbb4a33e5b1766bf530b1150bd0c/out-2021-5-15-17-59-8.jpg)

Generated from high speed footage of a CTA train out my window.

---

[![An interesting smear, vaguely resembling a lightsaber](https://bitbucket.org/grahampheath/slapse/raw/543e7b96e674f4d7d58497334de4344b43dc3634/demo/nJ0Vh0UGWVw.jpg)](https://bitbucket.org/grahampheath/slapse/raw/0e9d2ded662afbb4a33e5b1766bf530b1150bd0c/out-2021-5-18-17-15-36.jpg)

Generated from a [Geneva Drive video available on YouTube](https://www.youtube.com/watch?v=nJ0Vh0UGWVw).

---

[![Almost a Mercator projection map of the earth.](https://bitbucket.org/grahampheath/slapse/raw/543e7b96e674f4d7d58497334de4344b43dc3634/demo/wdSg9ROpvfI.jpg)](https://bitbucket.org/grahampheath/slapse/raw/0e9d2ded662afbb4a33e5b1766bf530b1150bd0c/out-2021-5-18-17-25-18.jpg)

Generated from a [Revolving Earth Globe available on YouTube](https://www.youtube.com/watch?v=wdSg9ROpvfI).


## Install

You'll need to have [ffmpeg](https://ffmpeg.org/) and [Image Magick](https://imagemagick.org/index.php) installed and available from the command line.

On OSX this is simple: 

    brew install ffmpeg imagemagick
	
Linux its likely very similar.

Windows has been tried with available binaries but getting Image Magick to appear in GitBash elluded us. FFMPEG seemed to work via the normal installer path.

## Use
```
$ node index.js --help
slapse [videoPath] [frames] [offset]

Positionals:
  videoPath, i  the video to make into art
                                    [string] [default: "./media/IMG_0095-7.MOV"]
  frames, f     how many frames to get (also the width of the output * slice
                width)                                           [default: 1000]
  offset, s     start slicing at this frame                         [default: 0]

Options:
      --help        Show help                                          [boolean]
      --version     Show version number                                [boolean]
  -r, --frameRate   Frame rate of source video, used to calculate start frame
                                                       [number] [default: 29.97]
  -z, --scale       (disabled) Scale factor of input video [number] [default: 1]
      --grabFrames  Whether to use ffmpeg to extract frames. Disable to avoid
                    duplicating effort when the frames have already been
                    extracted.                         [boolean] [default: true]
      --poolSize    How many threads to use for threaded tasks (currently: crop
                    and get info). FFMPEG handles its own threading config.
                                                                   [default: 10]
      --sliceWidth  How many pixel columns to use.                  [default: 1]
      --outPath     Path prefix for output.          [string] [default: "./out"]
      --outFormat   File format for extraction, cropping, and montaging. Note:
                    BMP produces higher quality results due to it being
                    lossless. JPG results in desaturation and artifacts.
                                                       [string] [default: "bmp"]
      --preview     Runs `open \$file` on the resulting image
                                                      [boolean] [default: false]

```

## Known issues

Old frames are in my new images!

- In order to support resuming frame extration files must have a predictabe name. This should be done with unique output names to avoid crossing streams, but has not yet been implemented. You can reset your output folders by deleting out and slice-out.

```
rm -rf out slice-out
```

or as a one liner, I often run this:

``` rm -rf out slice-out; time node index.js ```


