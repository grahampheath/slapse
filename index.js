const { spawn, spawnSync } = require('child_process');
const path = require('path');
const fs = require('fs');
const asyncPool = require('tiny-async-pool');

const {pad, getTimestamp} = require('./utils.js');

const DEBUG = false;
const DEBUG_ERROR = false;

const argv = require('yargs')
    .scriptName('slapse')
    .usage('$0 [videoPath] [frames] [offset]')
    .command('$0 [videoPath] [frames] [offset]', 'welcome to slapse!', (yargs) => {
        yargs.positional('videoPath', {
          type: 'string',
          default: './media/IMG_0095-7.MOV',
          describe: 'The video to make into art',
          alias: 'i',
        })
        .positional('frames', {
          type: 'integer',
          default: 1000,
          describe: 'How many frames to get (also the width of the output * slice width)',
          alias: 'f',
        })
        .positional('offset', {
          type: 'integer',
          default: 0,
          describe: 'Start slicing at this frame',
          alias: 's',
        })
        .option('sweep', {
          demandOption: false,
          default: false,
          describe: 'When enabled the origin of each slice becomes dynamic. Without it the origin is always the center of the frame',
          type: 'boolean',
        })
        .option('reverseSweep', {
          demandOption: false,
          default: false,
          describe: 'Reverses the direction of the sweep',
          type: 'boolean',
        })
        .option('frameRate', {
          alias: 'r',
          demandOption: false,
          default: 29.97,
          describe: 'Frame rate of source video, used to calculate start frame',
          type: 'number',
        })
        .option('scale', {
          alias: 'z',
          demandOption: false,
          default: 1,
          describe: '(disabled) Scale factor of input video',
          type: 'number',
        })
        .option('grabFrames', {
          demandOption: false,
          default: true,
          describe: 'Whether to use ffmpeg to extract frames. Disable to avoid duplicating effort when the frames have already been extracted.',
          type: 'boolean',
        })
        .option('poolSize', {
          demandOption: false,
          default: 10,
          describe: 'How many threads to use for threaded tasks (currently: crop and get info). FFMPEG handles its own threading config.',
          type: 'integer',
        })
        .option('sliceWidth', {
          demandOption: false,
          default: 1,
          describe: 'How many pixel columns to use.',
          type: 'integer',
        })
        .option('outPath', {
          demandOption: false,
          default: './out',
          describe: 'Path prefix for output.',
          type: 'string',
        })
        .option('outFormat', {
          demandOption: false,
          default: 'bmp',
          describe: 'File format for extraction, cropping, and montaging. Note: BMP produces higher quality results due to it being lossless. JPG results in desaturation and artifacts.',
          type: 'string',
        })
        .option('preview', {
          demandOption: false,
          default: false,
          describe: 'Runs `open \$file` on the resulting image',
          type: 'boolean',
        })
      }).argv

const debugLog = function(...msg) {
  if (DEBUG) console.log(msg.join('\n'))
};

const debugError = function(...msg) {
  if (DEBUG_ERROR) console.error(msg.join('\n'))
};

const setup = function() {
  spawnSync('mkdir', ['out']);
  spawnSync('mkdir', ['slice-out']);
}

const getFrameRangeAsImages = async function({videoPath, frames, offset, grabFrames, frameRate, outPath, outFormat}) {
  const promises = [];
  let fileNames = [];
  let outPaths = [];

  let data = '';
  return new Promise((resolve, reject) => {
    if (!grabFrames) {
      resolve(fs.readdirSync(outPath).map((image) => {
          return path.join(outPath, image);
        }));
      return;
    }

    const frameParam = `${offset / frameRate * 1000}ms`;

    const args = [
      // -ss coming before the -i makes the code faster, but sacrifices frame extraction on mpeg videos.
      // '-ss',
      // frameParam,
      '-i', videoPath,
      // -ss coming after the -i makes the code slower, but extracts every frame extraction on mpeg videos.
      '-ss',
      frameParam,
      '-frames:v', frames,
      // '-vf', `scale=iw*${SCALE}:ih*${SCALE}`,
      '-segment_start_number', '0',
      '-y',
      path.join(outPath, `out-%05d.${outFormat}`)];
    console.log(`spawn: ffmpeg [${frameParam}]: args ${args.join(' ')}`);
    const ffmpeg = spawn('ffmpeg', args);
    ffmpeg.stdout.on('data', (out) => {
      debugLog(`stdout: ${data}`);
      data += out;
    });

    ffmpeg.stderr.on('data', (out) => {
      debugError(`stderr: ffmpeg ${frameParam} ${out}`);
      data += out;
    });

    ffmpeg.on('close', (code) => {
      console.error(`stderr: ffmpeg ${frameParam} child process exited with code ${code}`);
      if (code == 0) {
        const images = fs.readdirSync(outPath).filter((image) => {
          return image != '.DS_Store';
        });
        resolve(images.map((image, index) => {
          return {
              index,
              totalImagesCount: images.length,
              imagePath: path.join(outPath, image),
          };
        }));
      } else {
        console.error(`stderr: ffmpeg ${frameParam} error:`, data)
        reject(data);
      }
    });
  })
}


const getInfo = async function(images, {poolSize}) {
  // convert out-0.jpg -format "%w,%h" info:
  const promises = []; 
  let cachedAnswer = null;
  let cachedAnswerString = null;
  return asyncPool(poolSize, images, (image) => {
    return new Promise((resolve, reject) => {
      if (cachedAnswer) {
        image.height = cachedAnswer.height;
        image.width = cachedAnswer.width;

        resolve(image);
        return;
      }

      const args = [
        image.imagePath, '-format', '%w,%h', 'info:',
      ];

      let data = '';
      let convert = spawn('convert', args);

      convert.stdout.on('data', (out) => {
        if (out && out != '' && out.toString != cachedAnswerString) {
          debugLog(`stdout:getInfo: ${data}`);
        }
        data += out;
      });

      convert.stderr.on('data', (out) => {
        if (out) {
          debugError(`stderr:getInfo: ${out}`);
        }
        data += out;
      });

      convert.on('close', (code) => {
        if (code == 0) {
          const [width, height] = data.split(',');
          if (!cachedAnswer) {
            cachedAnswer = {width, height};
            cachedAnswerString = data;
          }
          image.width = width;
          image.height = height;
          resolve(image);
        } else {
          debugError('convert error:', data)
          reject(data);
        }
      })
    });
  });
}


const cropFrames = async function(images, {poolSize, sliceWidth, outFormat, sweep, reverseSweep}) {
  // convert rose: -crop 40x30-10-10  crop_tl.gif
  const promises = [];
  const count = 0;
  return asyncPool(poolSize, images, (image) => {
    const height = image.height;
    const width = image.width;

    return new Promise((resolve, reject) => {
      let origin = Math.floor((width - (sliceWidth / 2)) / 2);
      if (sweep) {
        const progress = image.index/image.totalImagesCount;
        if (!reverseSweep) {
          origin = Math.floor(progress * width);
        } else {
          origin = Math.floor((1 - progress) * width);
        }
      }
      

      const args = [
        image.imagePath,
        '-crop',
        `${sliceWidth}x${height}+${origin}`,
        `slice-${image.imagePath.replace('.' + outFormat, '')}.${outFormat}`,
      ];

      let data = '';
      let convert = spawn('convert', args);
      convert.stdout.on('data', (out) => {
        debugLog(`stdout:cropFrames: ${data}`);
        data += out;
      });

      convert.stderr.on('data', (out) => {
        debugError(`stderr:cropFrames: ${out}`);
        data += out;
      });
      convert.on('close', (code) => {
        if (code == 0) {
          const [width, height] = data.split(',');
          resolve()
        } else {
          reject(data)
          debugError('convert error:', data)
        }
      })
    })
  })
}

const buildMontage = function(images, {offset, outFormat}) {
  const howMany = images.length;
  const outName = `${path.parse(argv.videoPath).name}-${getTimestamp()}.${outFormat}`
  const args = [`slice-out/out-%05d.${outFormat}[${offset + 1}-${(offset) + howMany}]`,
    '-tile', `${howMany}x1`,
    '-geometry', '+0+0',
    outName,
  ];

  return new Promise((resolve, reject) => {
    debugLog(`montage: args: ${args}`);
    const montage = spawn('montage', args);

    let data = '';
    montage.stdout.on('data', (out) => {
      debugLog(`stdout: ${data}`);
      data += out;
    });

    montage.stderr.on('data', (out) => {
      debugError(`stderr: ${out}`);
      data += out;
    });

    montage.on('close', (code) => {
      if (code == 0) {
        const [width, height] = data.split(',');
        resolve(outName)
        console.log(`montage ${args.join(' ')}\n done!`);
      } else {
        reject()
        console.log('montage error: ', data)
      }
    });
  });
}

const main = async (argv) => {
  console.log(`[1/5] Building video [${argv.videoPath}] total frames [${argv.frames}], beginning with [${argv.offset}]`);
  setup();
  const images = await getFrameRangeAsImages(argv);
  console.log(`[2/5] Extracted ${images.length} images.`);
  const imageSizes = await getInfo(images, argv);
  console.log('[3/5] Computed image sizes');
  await cropFrames(imageSizes, argv);
  console.log('[4/5] Cropped frames');
  const outFileName = await buildMontage(images, argv);
  console.log('[5/5] Montage built');
  console.log('');
  console.log(`Image path: ${path.join('./', outFileName)}`);
  if (argv.preview) {
    const outPath = path.join('./', outFileName);
    console.log(`Opening preview with \`open ${outPath}\``);
    spawn('open', [outPath]);
  }
}

main(argv);
