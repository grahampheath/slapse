const pad = function(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

module.exports = {
  pad,
  getTimestamp: function() {
    const date = new Date(),
    dateValues = [
      date.getFullYear(),
      '_',
      pad(date.getMonth() + 1, 2),
      '_',
      pad(date.getDate(), 2),
      'T',
      pad(date.getHours(), 2),
      '_',
      pad(date.getMinutes(), 2),
      '_',
      pad(date.getSeconds(), 2),
    ];
  
    return dateValues.join('');
  },
}